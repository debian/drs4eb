# README #

### DRS4 Evaluation Board firmware and software ###

* VHDL firmware for Xilinx ISE
* **drscl** command line interface to DRS4 Evaluation Board
* **drsosc** oscilloscope program for DRS4 Evaluation Board

### Contact ###

Stefan Ritt <stefan.ritt@psi.ch>